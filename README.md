Advent Of Code 2020 Solutions using Rust

Day 1:
Besides not being able to get my input for a couple minutes, solutions wasn't too hard to implement. Just finished cleaning it up a little this morning.

Day 2:
Biggest roadblock was relearning all the ways to split strings in Rust. Better to learn it now than later in the challenge, I guess. I don't want to, but I should prob clean it up a little just to get me in some better Rust habits.

Day 3:
First part wasn't too bad, just took a bit to implement. Part two challenged my lazy coding in part one and I had to rewrite a lot of it. Finished right around the 2-hour mark.
